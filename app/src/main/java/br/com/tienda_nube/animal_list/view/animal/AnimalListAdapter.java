package br.com.tienda_nube.animal_list.view.animal;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.tienda_nube.animal_list.R;
import br.com.tienda_nube.animal_list.model.Animal;

/**
 * Created by Thialyson on 17/11/18.
 */

public class AnimalListAdapter extends RecyclerView.Adapter<AnimalListAdapter.ViewHolder> {

    private List<Animal> animalList;
    private LayoutInflater layoutInflater;

    public AnimalListAdapter(List<Animal> animalList, Context context) {
        if(context != null) {
            this.animalList = animalList;
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.animal_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Picasso.get()
                .load(animalList.get(position).getUrl())
                .fit()
                .centerCrop()
                .into(holder.imageAnimal);
    }


    @Override
    public int getItemCount() {
        return animalList.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageAnimal;

        ViewHolder(final View itemView) {
            super(itemView);

            imageAnimal = (ImageView) itemView.findViewById(R.id.imageAnimal);
        }
    }

}

