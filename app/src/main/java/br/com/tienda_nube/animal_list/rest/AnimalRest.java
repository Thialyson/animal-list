package br.com.tienda_nube.animal_list.rest;

import java.util.List;

import br.com.tienda_nube.animal_list.model.Animal;
import br.com.tienda_nube.animal_list.util.Utils;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Thialyson on 17/11/18.
 */

public interface AnimalRest {


    @Headers(Utils.X_API_KEY + ":" + Utils.X_API_VALUE_CAT)
    @GET("/v1/images")
    Call<List<Animal>> requestCats(
            @Query("limit") Integer limit,
            @Query("page") Integer page
    );


    @Headers(Utils.X_API_KEY + ":" + Utils.X_API_VALUE_DOG)
    @GET("/v1/images")
    Call<List<Animal>> requestDogs(
            @Query("limit") Integer limit,
            @Query("page") Integer page
    );


}
