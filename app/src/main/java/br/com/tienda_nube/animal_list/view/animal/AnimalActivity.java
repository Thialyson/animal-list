package br.com.tienda_nube.animal_list.view.animal;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import br.com.tienda_nube.animal_list.R;
import br.com.tienda_nube.animal_list.view.cat.CatListFragment_;
import br.com.tienda_nube.animal_list.view.dog.DogListFragment_;

@EActivity(R.layout.activity_animal)
public class AnimalActivity extends AppCompatActivity {

    @ViewById(R.id.tabLayout)
    TabLayout tabLayout;

    @ViewById(R.id.viewPager)
    ViewPager viewPager;

    @ViewById(R.id.barLayout)
    AppBarLayout appBarLayout;

    ViewPagerAdapter viewPagerAdapter;

    @AfterViews
    void afterViews(){

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        addFragments();
    }

    void addFragments(){
        viewPagerAdapter.addFragments(new CatListFragment_(), "GATOS");
        viewPagerAdapter.addFragments(new DogListFragment_(), "CACHORROS");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }

}
