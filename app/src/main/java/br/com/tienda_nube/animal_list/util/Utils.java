package br.com.tienda_nube.animal_list.util;

import br.com.tienda_nube.animal_list.rest.AnimalRest;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Thialyson on 17/11/18.
 */

public class Utils {

    public static final String CAT_URL = "https://api.thecatapi.com/";
    public static final String DOG_URL = "https://api.thedogapi.com/";
    public static final String X_API_KEY = "x-api-key";
    public static final String X_API_VALUE_CAT = "279cc9d9-9c3e-413f-9546-d232dcad3ea3";
    public static final String X_API_VALUE_DOG = "f711cb7a-9d71-48af-b9ff-c3ac932a2b7d";


    public static AnimalRest getService(String animal) {

        OkHttpClient client = new OkHttpClient.Builder()
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getUrl(animal))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit.create(AnimalRest.class);
    }

    private static String getUrl(String animal){

        String response = null;

        switch (animal){
            case "cat":
                response = CAT_URL;
                break;
            case "dog":
                response = DOG_URL;
            break;
        }

        return response;
    }
}
