package br.com.tienda_nube.animal_list.model;

/**
 * Created by Thialyson on 17/11/18.
 */

public class Animal {

    private String id;

    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
