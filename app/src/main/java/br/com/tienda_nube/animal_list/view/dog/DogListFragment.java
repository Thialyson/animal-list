package br.com.tienda_nube.animal_list.view.dog;

import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import br.com.tienda_nube.animal_list.R;
import br.com.tienda_nube.animal_list.model.Animal;
import br.com.tienda_nube.animal_list.rest.AnimalRest;
import br.com.tienda_nube.animal_list.util.Utils;
import br.com.tienda_nube.animal_list.view.animal.AnimalListAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@EFragment(R.layout.fragment_dog_list)
public class DogListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    @ViewById(R.id.swipeRefreshDog)
    SwipeRefreshLayout swipeRefresh;

    @ViewById(R.id.dogsRecyclerView)
    RecyclerView dogsRecyclerView;

    @ViewById(R.id.progressBarDog)
    ProgressBar progressBarDog;

    @ViewById(R.id.tvNoDogs)
    TextView tvNoDogs;

    AnimalRest animalRest;

    List<Animal> animalList;

    @AfterViews
    void afterViews() {

        swipeRefresh.setOnRefreshListener(this);
        listenFeedBackClick();
        animalRest = Utils.getService(getString(R.string.dog));
        requestDogs();
    }

    void requestDogs() {

        animalRest.requestDogs(50, 0).enqueue(new Callback<List<Animal>>() {
            @Override
            public void onResponse(Call<List<Animal>> call, Response<List<Animal>> response) {

                loadList(response);
            }

            @Override
            public void onFailure(Call<List<Animal>> call, Throwable t) {

                showFeedBack();
                tvNoDogs.setText(R.string.no_connecion);
                t.printStackTrace();
            }
        });
    }

    void loadList(Response<List<Animal>> response){
        if (getActivity() != null) {

            animalList = new ArrayList<>();

            if (response.body() == null || response.body().size() == 0) {
                showFeedBack();
            } else {
                hideProgress();
                animalList.addAll(response.body());
                setAdapterRecyclerView();
            }
        }
    }

    void showFeedBack(){
        hideProgress();
        tvNoDogs.setVisibility(View.VISIBLE);
    }

    void hideProgress(){
        progressBarDog.setVisibility(View.GONE);
    }

    void setAdapterRecyclerView() {

        AnimalListAdapter animalListAdapter = new AnimalListAdapter(animalList, getActivity());
        dogsRecyclerView.setAdapter(animalListAdapter);
        animalListAdapter.notifyDataSetChanged();
    }


    @Override
    public void onRefresh() {
        requestDogs();
        swipeRefresh.setRefreshing(false);
    }

    void listenFeedBackClick(){
        tvNoDogs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestDogs();
            }
        });
    }
}
